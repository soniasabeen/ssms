SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[test_table] (
		[id]            [int] IDENTITY(1, 1) NOT NULL,
		[name]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ipaddress]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[test_table] SET (LOCK_ESCALATION = TABLE)
GO
